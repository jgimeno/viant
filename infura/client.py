from web3 import Web3


class Client:
    def __init__(self, node_address):
        self.node_address = node_address

    def get_block_and_transaction_hash_by_address(self, contract_address):
        provider = Web3.HTTPProvider(self.node_address)
        w3 = Web3(provider)

        deployed_block_number = get_deployed_block_number(w3, contract_address)
        return get_deployment_hash_from_block(w3, contract_address, deployed_block_number)


def get_latest_block_number(w3):
    latest_block = w3.eth.getBlock('latest')
    latest_block_number = latest_block.number
    return latest_block_number


def code_is_empty(code):
    return code == b''


def get_contract_code_at_block(w3, contract_address, midpoint):
    return w3.eth.getCode(w3.toChecksumAddress(contract_address), midpoint)


def get_deployed_block_number(w3, contract_address):
    first = 0
    last = get_latest_block_number(w3)
    found = False
    deployed_block_number = None
    while first <= last and not found:
        midpoint = (first + last) // 2
        code_current_block = get_contract_code_at_block(w3, contract_address, midpoint)

        if not code_is_empty(code_current_block):
            last = midpoint - 1
            continue

        code_next_block = get_contract_code_at_block(w3, contract_address, midpoint + 1)
        if code_is_empty(code_current_block) and not code_is_empty(code_next_block):
            found = True
            deployed_block_number = midpoint + 1
        else:
            first = midpoint + 1

    return deployed_block_number


def get_deployment_hash_from_block(w3, contract_address, block_number):
    block = w3.eth.getBlock(block_number, True)

    for tx in block["transactions"]:
        tx_is_contract_deploy = tx["to"] is None
        if tx_is_contract_deploy:
            receipt = w3.eth.getTransactionReceipt(tx["hash"])
            if hex(int(receipt['contractAddress'], 16)) == hex(int(contract_address, 16)):
                return ContractDeployment(tx["hash"].hex(), block['hash'].hex())

    return None


class ContractDeployment:
    def __init__(self, tx_hash, block_hash):
        self.tx_hash = tx_hash
        self.block_hash = block_hash

    def get_tx_hash(self):
        return self.tx_hash

    def get_block_hash(self):
        return self.block_hash
