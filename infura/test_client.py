from unittest import TestCase
from infura.client import Client, get_deployed_block_number, get_deployment_hash_from_block, ContractDeployment
from web3 import Web3

NODE_ADDRESS = "A NODE ADDRESS"


class TestClient(TestCase):
    def test_it_returns_transaction_hash_when_using_contract_address(self):
        client = Client(NODE_ADDRESS)

        hashes = client.get_block_and_transaction_hash_by_address("0x6c8f2a135f6ed072de4503bd7c4999a1a17f824b")
        expected = ContractDeployment(
            "0x47c8f460e9f499b4d57f81d00d6ead421ec9a937af3d8b646e6c2264a43cbfce",
            "0xcd4cd7e70a5b64b5046107e22876d758b9e16a2b4e305874d5bfa50802e7065d"
        )

        self.assertEqual(expected.get_block_hash(), hashes.get_block_hash())
        self.assertEqual(expected.get_tx_hash(), hashes.get_tx_hash())

    def test_get_deployed_block_number(self):
        provider = Web3.HTTPProvider(NODE_ADDRESS)
        w3 = Web3(provider)

        self.assertEqual(930481, get_deployed_block_number(w3, "0x6c8f2a135f6ed072de4503bd7c4999a1a17f824b"))
        self.assertEqual(6538467, get_deployed_block_number(w3, "0x94b35101cdb509ebdcee626b599d4327b369f73c"))

    def test_get_tx_hash_of_contract_deployed_from_block(self):
        provider = Web3.HTTPProvider(NODE_ADDRESS)
        w3 = Web3(provider)

        expected = ContractDeployment(
            "0x47c8f460e9f499b4d57f81d00d6ead421ec9a937af3d8b646e6c2264a43cbfce",
            "0xcd4cd7e70a5b64b5046107e22876d758b9e16a2b4e305874d5bfa50802e7065d"
        )
        hashes = get_deployment_hash_from_block(w3, "0x6c8f2a135f6ed072de4503bd7c4999a1a17f824b", 930481)

        self.assertEqual(expected.get_block_hash(), hashes.get_block_hash())
        self.assertEqual(expected.get_tx_hash(), hashes.get_tx_hash())
