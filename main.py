import argparse
import sys
from infura.client import Client


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("address", help="The contract address to get the hashes")
    parser.add_argument("--host", help="The host node that is needed to connect", default="")
    args = parser.parse_args()

    if args.host == "":
        print("host is mandatory")
        sys.exit(2)

    print("searching...")

    client = Client(args.host)
    hashes = client.get_block_and_transaction_hash_by_address(args.address)
    print("Block: ", hashes.get_block_hash())
    print("Transaction: ", hashes.get_tx_hash())


if __name__ == "__main__":
    main()
