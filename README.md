### How to use

To get the block hash and tx hash of the block that the contract was deployed 
we can use.

```
python3.7  main.py address --host https://mainnet.infura.io/v3/etc
```

Where host is a valid ethereum node throught http and address is a contract address.
